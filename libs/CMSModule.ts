import merge from 'lodash/merge';
import API from '@/api';
/* eslint-disable import/no-cycle */
import { cmsModules } from '@/index';
import { Schema, Config } from '@/types';

interface QueryParams {
  query: string;
  page: number;
  pageSize: number;
  creator?: string;
  modifier?: string;
  link?: string;
  selectedLan?: string;
  selectedLanStatus?: string;
}

interface CreateParams {
  title: string;
  platform: string;
  supportLanguages: string[];
  urlKey: string;
  link?: string;
}

interface UpdateParams {
  platform?: string;
  title?: string;
  link?: string;
}

function onRes(res: any) {
  if (!res.success) {
    throw new Error(res.error.message);
  } else return res.result;
}

export default class CMSModule {
  key!: string;
  schema!: Schema;

  constructor(key: string, schema: Schema) {
    // 单例模式
    if (cmsModules[key]) return cmsModules[key];
    if (!(this instanceof CMSModule)) {
      return new CMSModule(key, schema);
    }
    this.key = key;
    this.schema = schema;
    cmsModules[key] = this;
  }

  // 获取模块配置列表
  async getConfigList (params: QueryParams): Promise<Array<Config>> {
    const configList = await API.getConfigList(this.key, {
      page: params.page,
      page_size: params.pageSize,
      search_text: params.query,
      create_user: params.creator,
      edit_user: params.modifier,
      link: params.link,
      selected_language: params.selectedLan,
      select_status: params.selectedLanStatus,
    }).then(onRes).then((result: any) => {
      // 由于后端接口命名不太规范，这里做一层转换
      result.data = result.data.map((item: any) => {
        return {
          id: item.id,
          module: item.topic,
          // platform: item.platform,
          title: item.title,
          creator: item.create_user,
          createAt: item.create_time,
          modifier: item.edit_user,
          modifyAt: item.last_modify_time,
          urlKey: item.url_key,
          link: item.link,
          publishedLanguages: item.publish_language,
          selectedLanStatus: item.select_language_status,
        } as Config;
      });
      // debugger;
      return result;
    });
    return configList;
  }

  // 创建模块配置
  createConfig (params: CreateParams) {
    return API.createConfig(this.key, {
      title: params.title,
      platform: '',
      allow_language: params.supportLanguages,
      url_key: params.urlKey,
      link: params.link,
    }).then(onRes);
  }

  // 更新模块配置
  updateConfig (configId: number, params: UpdateParams) {
    return API.updateConfig({
      content_id: configId,
      ...params
    }).then(onRes);
  }

  // 删除模块配置
  deleteConfig (configId: number) {
    return API.deleteConfig(configId).then(onRes);
  }

  // 获取模块配置在指定语言下的配置数据
  getConfigData (configId: number, editLanguage: string, refLanguage: string = editLanguage) {
    return Promise.all([
      this.getConfigLanData(configId, "all"),
      this.getConfigLanData(configId, editLanguage),
      refLanguage === editLanguage
        ? Promise.resolve(undefined)
        : this.getConfigLanData(configId, refLanguage)
    ]).then((res) => {
      /* eslint-disable prefer-const */
      let [comJSONStr, editLanJSONStr, refLanJSONStr] = res;
      if (refLanJSONStr === undefined) refLanJSONStr = editLanJSONStr;

      const value = comJSONStr
        ? merge(
          JSON.parse(comJSONStr),
          editLanJSONStr ? JSON.parse(editLanJSONStr) : {}
        )
        : undefined;

      const _refValue = refLanJSONStr ? JSON.parse(refLanJSONStr) : undefined;

      return [value, _refValue];
    });
  }

  // 获取模块配置在指定语言下的配置数据
  async getConfigLanData (configId: number, language: string) {
    return await API.getLanConfigData(configId, language)
      .then(onRes)
      .then(res => {
        return res.data ? res.data.text_draft : undefined;
      });
  }

  // 更新模块配置在指定语言下的配置数据
  async updateConfigData (configId: number, language: string, data: Object, lanData: Object) {
    // 更新通用配置
    await API.updateLanConfigData(configId, "all", JSON.stringify(data))
      .then(onRes);

    // 更新语言配置
    await API.updateLanConfigData(
      configId,
      language,
      JSON.stringify(lanData)
    ).then(onRes);
  }

  // 发布模块配置数据
  async publishConfigData (configId: number, language: string) {
    // 检查配置数据是否为空
    const configData = await this.getConfigLanData(configId, language);
    if (!configData) {
      throw new Error('config data is empty');
    }
    await API.publishLanConfigData(configId, "all").then(onRes);
    await API.publishLanConfigData(configId, language).then(onRes);
  }

  // 取消发布(下架)模块配置数据
  async unPublishConfigData (configId: number, language: string) {
    await API.unpublishLanConfigData(configId, "all").then(onRes);
    await API.unpublishLanConfigData(configId, language).then(onRes);
  }

  // 获取模块配置支持的语言列表
  async getConfigSupportLanguages (configId: number) {
    return await API.getConfigSupportLanList(configId).then(onRes);
  }

  injectDataIntoSchema (data: any) {
    const [value, refValue] = data;
    this.schema.value = value;
    this.schema._refValue = refValue;
  }
}
