import axios from 'axios';
import { querystring } from '@/utils';

const API_PREFIX = "/v1/mpserv/v1/cmsserv";

axios.interceptors.response.use((response) => {
  return response.data;
});

// make a http adaptor which maybe more flexible,
// cause we may use Axios or other http libs to replace it.
let $http = {
  get(url: string, data = {}, config = {}) {
    return axios.get(url + '?' + querystring.stringify(data), config);
  },
  post(url: string, data = {}, config = {}) {
    return axios.post(url, data, config);
  }
};

export function setHttpAdaptor (httpAdaptor: {
  get: (url: string, data: any) => any,
  post: (url: string, data: any, config?: any) => any,
}) {
  $http = httpAdaptor;
}

// ======== API ========
// backend API interface desc can be saw at:
// partership-cms: https://orion.klook.io/interface/interfaces/detail/13/443/6341/show

/**
 * get config list of module
 * @param {number} moduleName name of module
 * @param {Object} query
 * @param {string} query.search_text keyword, support id, title query
 * @param {number} query.page current page
 * @param {number} query.page_size page size
 * @returns list of module config
 */
async function getConfigList(moduleName: string, query: any) {
  return await $http.get(`${API_PREFIX}/content/list`, {
    topic: moduleName,
    ...query
  });
}

/**
 * create a module config
 * @param {string} moduleName name of module
 * @param {Object} config
 * @param {string} config.title title of module
 * @param {string} config.platform module config support platform
 * @param {Array[string]} config.allow_language module config support languages
 * @param {string} config.url_key a key appended to url as query for index
 * @param {string} config.link a field preserved for relative usage
 */
async function createConfig(moduleName: string, config: any) {
  return await $http.post(`${API_PREFIX}/content/create`, {
    topic: moduleName,
    ...config
  });
}

/**
 * update a config of module
 * @param {Object} config
 * @param {string} config.content_id id of a config
 * @param {string} config.title title of module
 * @param {string} config.platform module config support platform
 * @param {string} config.link a field preserved for relative usage
 */
async function updateConfig(config: any) {
  return await $http.post(`${API_PREFIX}/content/update`, config);
}

/**
 * remove a config of module
 * @param {number} configId ID of config
 */
async function deleteConfig(configId: number) {
  return await $http.post(`${API_PREFIX}/content/delete`, {
    content_id: configId
  });
}

// 查询配置支持的语言列表
/**
 * get language status list of a module config
 * @param {number} configId config id
 */
async function getConfigSupportLanList(configId: number) {
  return await $http.get(`${API_PREFIX}/content/language_status`, {
    content_id: configId
  });
}

// 获取特定语言配置数据
/**
 * get data of a module config with specified language
 * @param {number} content_id config id
 * @param {string} language specified language
 */
async function getLanConfigData(content_id: number, language = "all") {
  return await $http.get(`${API_PREFIX}/resource/show`, {
    content_id,
    language
  });
}

// 更新模块特定语言数据
/**
 * update specified language data of a module config
 * @param {number} content_id config id
 * @param {string} language specified language
 * @param {string} text_draft config draft data
 */
async function updateLanConfigData(content_id: number, language: string, text_draft: string) {
  return await $http.post(`${API_PREFIX}/resource/save`, {
    content_id,
    language,
    text_draft
  });
}

// 发布模块特定语言数据
/**
 * publish specified language data of a module config
 * @param {number} content_id config id
 * @param {string} language specified language
 */
async function publishLanConfigData(content_id: number, language: string) {
  return await $http.post(`${API_PREFIX}/resource/publish`, {
    content_id,
    language
  });
}

// 取消发布模块特定语言数据
/**
 * cancle publish specified language data of a module config
 * @param {number} content_id config id
 * @param {string} language specified language
 */
async function unpublishLanConfigData(content_id: number, language: string) {
  return await $http.post(`${API_PREFIX}/resource/unpublish`, {
    content_id,
    language
  });
}

export default {
  // 配置
  getConfigList,
  createConfig,
  updateConfig,
  deleteConfig,
  getConfigSupportLanList,
  // 配置内容
  getLanConfigData,
  updateLanConfigData,
  publishLanConfigData,
  unpublishLanConfigData
};
