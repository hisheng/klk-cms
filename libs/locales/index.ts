import zh_CN from './zh_CN.json';
import en_US from './en_US.json';

export default {
  zh_CN,
  en_US
};
