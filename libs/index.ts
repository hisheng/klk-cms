/**
 * @author Hisheng
 * @description A Simple CMS System for KLOOK
 * @depedencies Vue, ElementUI, simpleMDE, Lodash, cloudinary
 */
import Vue from "vue";
import VueI18n from 'vue-i18n';
import merge from 'lodash/merge';
// import ElementUI from 'element-ui';
// import locale from 'element-ui/lib/locale/lang/en';
import { setHttpAdaptor } from "@/api";
import { mergeCMSOptions } from "@/utils";
import locales from '@/locales';
import Entry from "@/pages/entry.vue";
/* eslint-disable import/no-cycle */
import CMSModule from '@/CMSModule';

export { default as SchemaRender } from '@/components/render.vue';

Vue.use(VueI18n);
// Vue.use(ElementUI, { locale });

export { default as API } from "./api";

// 保存全局所有的 CMS 模块
export const cmsModules: {
  [key: string]: CMSModule
} = {};

/* eslint-disable import/no-cycle */
export { default as CMSModule } from './CMSModule';

function getModules() {
  return cmsModules;
}

function getModule(key: string) {
  return cmsModules[key];
}

// render a CMS Module on a specified Element.
function render (module: CMSModule, el: string, options: any = {}) {
  console.assert(typeof module !== 'undefined', 'please provide a CMS Module.');
  console.assert(typeof el !== 'undefined', 'please provide a Element for Module to mount on.');
  /* const i18n = new VueI18n({
    locale: options.locale || 'zh_CN',
    messages: locales,
  }); */
  const lang = options.locale || 'zh_CN';
  // @ts-ignore
  Vue.config.lang = lang;
  // @ts-ignore
  Vue.locale(lang, locales[lang]);
  return new Vue({
    extends: Entry,
    // i18n,
    propsData: {
      module,
      options: mergeCMSOptions(options)
    }
  }).$mount(el);
}

function getLocale (lan: string) {
  return (locales as any)[lan];
}

function setLocale (lan: string, locale: any) {
  if ((locales as any)[lan]) {
    (locales as any)[lan] = merge((locales as any)[lan], locale);
  } else (locales as any)[lan] = locale;
}

function getLocales () {
  return locales;
}

const CMS = {
  render,
  getModules,
  getModule,
  setHttpAdaptor,
  getLocale,
  setLocale,
  getLocales,
};

export default CMS;
