const path = require('path');
const merge = require('lodash/merge');
const baseConfig = require('./webpack-base.config.js');
const webpack = require('webpack');
// const BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin;

const prodConfig = merge(baseConfig, {
  mode: 'production',
  output: {
    library: 'CMS',
    libraryTarget: 'commonjs2'
  },
  externals: {
    vue: {
      commonjs: 'vue',
      commonjs2: 'vue',
      amd: 'vue',
      root: 'Vue',
    },
    'element-ui': 'element-ui'
  },
  performance: {
    hints: false
  },
  stats: {
    children: false
  },
  optimization: {
    minimize: false
  },
});

prodConfig.plugins = [
  ...prodConfig.plugins,
  // new BundleAnalyzerPlugin(),
  new webpack.optimize.LimitChunkCountPlugin({
    maxChunks: 1
  }),
];

module.exports = prodConfig;