# Klook-CMS

主要用于 klook-admin 页面配置的 CMS 系统。

`CMS`：Configurable Module System.

基于数据实现模块的可配置化。

## 介绍

CMS 系统的相关介绍及概念参见文档：[CMS 设计方案](https://klook.slab.com/posts/cms-%E8%AE%BE%E8%AE%A1%E6%96%B9%E6%A1%88-b9yoytq6)

## 使用

安装

```js
npm i @klook/klk-cms -S
```

使用

```js
import CMS, { CMSModule } from "@klook/klk-cms";
import schema, { fetchData } from "./schema.js";

const moduleKey = "CAMPAIGN_PAGE";

const options = {
  pages: {
    edit: {
      beforeRender: async function() {
        // fetch async data before render
        await fetchData();
        return true;
      }
    }
  }
};

// create a CMS Module
const campaignPage = new CMSModule(moduleKey, schema);
// render a CMS Module on a specified Element
const vueIns = CMS.render(campaignPage, "#app", options);
```

如果只是想使用表单生成功能，可以只引用 `schema-render` 组件：

```js
import { schemaRender } from "@klook/klk-cms";

Vue.component("schema-render", schemaRender);
```

在模板中使用

```html
<template>
  <schema-render :schema="schema"></schema-render>
</template>
```

## Schema
`Schema` 是一个渲染描述对象，通过一个 `schema` 告诉 CMS 页面应该渲染哪些组件，以及组件间如何交互。

以下是一个简单的 `schema` 定义：
```js
const schema: Schema = {
  title: "Campaign Page Settings",
  components: [
    {
      type: "text",
      title: "Page Title",
      key: "page_title",
      value: "",
      multiLanguage: true,
      rules: [{ required: true, message: INVALID_MSG, trigger: "blur" }]
    },
    // color tone
    {
      type: "select",
      title: "Color Tone",
      key: "color_tone",
      value: null,
      options: [
        { label: "Regular White", value: "regularWhite" },
        { label: "Pine Green", value: "pineGreen" },
        { label: "Persian Blue", value: "persianBlue" },
        { label: "Brand Orange", value: "brandOrange" },
        { label: "Solid Black", value: "solidBlack" }
      ]
    },
  ],
  onChange ({ component }) {
    // 监听组件变化
  }
};
```
`schema` 基本上是由组件定义组成的，具体的组件定义支持见：[Components](./docs/components.md)


## 多语言

如何设置多语言见：[Multi-Languages](./docs/multi-languages.md)


## API
**Warning**: 以下接口暂未正式发布，仍在测试中

API 层次结构如下：

- CMS
  - CMSModule `Class`

  - render `Function`

  - getModules `Function`

  - getLocales `Function`

  - getLocale `Function`

  - setLocale `Function`

详细定义见：[API](./docs/api.md)


## 优缺点

优点

- 减少重复冗余的页面与表单创建过程

- 保持一致的表单风格与布局

- 支持多语言

- 满足大部分需求的实现

- 可嵌套表单

- 支持自定义表单验证，嵌套验证

- 支持自定义组件


缺点

- 丧失样式与布局的灵活性

- 无法满足所有需求，特殊需求仍然需要单独实现页面和接口

## TODO

- [x] Use TypeScript.
- [x] Make `upload`, `markdown` Components Independent.
- [ ] ~~Use higher version of `VueI18n`.~~
  
  > 由于 admin 环境受限，使用不同版本会有问题

- [ ] ~~Use higher version of `ElementUI`.~~

  > 由于 admin 环境受限，使用不同版本会有问题
  
- [ ] Make exposed interfaces confirmed.
- [ ] Make `schema-render` component can be used individually.
- [ ] Fullfill relative docs and examples.
- [ ] UI, UX Design.
- [ ] Replace `simplemde` with another markdown editor.
