import CMS, { CMSModule } from '../../libs';
import schema from './schema';

const moduleKey = 'CAMPAIGN_PAGE';

const campaign = new CMSModule(moduleKey, schema);

CMS.render(campaign, '#app', {
  locale: 'en_US',
  /* pages: {
    list: {
      filter: {
        immediateFilter: true
      }
    }
  } */
});

// console.log('>>> locales', CMS.getLocales());
console.log('>>> locale.zh_CN', CMS.getLocale('zh_CN'));
