const path = require('path');
const webpack = require('webpack');
const analyzer = require('webpack-bundle-analyzer');

const isDev = process.env.NODE_ENV === 'development';

const template = path.join(__dirname, '/tests/demo/index.html');

const cloudinary = path.join(__dirname, '/libs/cloudinary-v2.0.min.js');

module.exports = {
  css: {
    extract: false,
  },
  devServer: {
    proxy: {
      '^/v1': {
        target: 'http://admind137.debug.klook.io',
        ws: true,
        changeOrigin: true
      },
    }
  },
  configureWebpack: {
    output: isDev ? {} : {
      filename: 'klk-cms.common.js',
      library: 'CMS',
      libraryTarget: 'commonjs2',
    },
    externals: {
      vue: isDev ? 'Vue' : {
        commonjs: 'vue',
        commonjs2: 'vue',
        amd: 'vue',
        root: 'Vue',
      },
      'element-ui': isDev ? 'ELEMENT' : 'element-ui'
    },
    resolve: {
      alias: {
        cloudinary,
        '@': path.join(__dirname, '/libs')
      }
    },
    /* optimization: {
      minimize: false,
    } */
  },
  chainWebpack: config => {
    if (isDev) {
      config
        .plugin('html')
        .tap(args => {
          args[0].template = template;
          return args;
        });
    } else {
      /* const limitChunkPlugin = new webpack.optimize.LimitChunkCountPlugin({
        maxChunks: 1
      }); */

      /* config
        .plugin('modile-concatenation')
        .use(new webpack.optimize.ModuleConcatenationPlugin()); */

      config
        .plugins
        .delete('html')
        .delete('preload')
        .delete('prefetch')
        .end();

      // config
      //   .plugin('webpack-bundle-analyzer')
      //   .use(analyzer.BundleAnalyzerPlugin);
    }
  }
};
