
const path = require('path');
const merge = require('lodash/merge');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const baseConfig = require('./webpack-base.config.js');

const template = path.join(__dirname, '/tests/demo/index.html');

const devConfig = merge(baseConfig, {
  mode: 'development',
  entry: {
    app: path.join(__dirname, 'tests/demo/index.ts')
  },
  externals: {
    vue: 'Vue',
    'element-ui': 'element-ui'
  },
  devServer: {
    contentBase: path.join(__dirname, '/dist'),
    host: 'localhost',
    port: '8686',
    compress: true,
    hot: true,
    proxy: {
      '^/v1': {
        target: 'http://admind137.debug.klook.io',
        ws: true,
        changeOrigin: true
      },
    }
  },
});

devConfig.plugins = [
  ...devConfig.plugins,
  new HtmlWebpackPlugin({
    template,
  })
];

module.exports = devConfig;