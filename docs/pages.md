## Pages
当使用 `CMS.render(module: CMSModule, el: Selector, options?: RenderOptions)` 将一个模块渲染到特定节点时，会在该节点上渲染出一个模块的配置**列表页**，通过列表页可以进入**创建页**和**编辑页**。不同页面的行为可以通过 `options` 参数进行配置。

### 页面

#### 列表页
此页面是一个`模块配置`的列表页，支持筛选，分页等基本功能。

#### 创建页
通过此页面可以创建一个新的`模块配置`。

#### 编辑页
通过此页面可以对某一个模块配置的具体`配置数据`进行修改。

### RenderOptions
`CMS.render` API 的一个配置项，支持的配置如下：

#### singleLanguage

#### pages

##### list

- filter

    - immediateFilter 是否无需查询按钮，当筛选项变化时立即发起查询

    - schema 筛选区域表单的渲染 schema

- columns 列表表格的 columns，可以直接指定一个    columns 或者通过函数对原来的 columns 进行修改，返回新的 columns:

```js
{
  columns (cols) {
    return cols.map(col => {
      // do something here
      return col;
    });
  }
}
```

- beforeRender 钩子函数，在列表页渲染前执行，可以通过返回 `true` 或者 `false` 决定是否继续渲染，也可以定义一个 async 函数在获取完异步数据后再返回 `true` 进行渲染。

##### create

- schema 创建表单的渲染 schema

- beforeRender 钩子函数，在创建页渲染前执行，可以通过返回 `true` 或者 `false` 决定是否继续渲染，也可以定义一个 async 函数在获取完异步数据后再返回 `true` 进行渲染。

##### edit

- beforeRender 钩子函数，在编辑页渲染前执行，可以通过返回 `true` 或者 `false` 决定是否继续渲染，也可以定义一个 async 函数在获取完异步数据后再返回 `true` 进行渲染。

##### 配置示例
```js
import { fetchData } from './schema';

const options = {
  pages: {
    list: {
      async beforeRender () {
        // 先获取异步数据
        await fetchData();
        // 继续执行渲染
        return true;
      }
    }
  }
};

CMS.render(module, '#app', options);
```