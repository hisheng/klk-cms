## API

### `CMSModule`
CMS 模块类

构造函数：

`CMSModule(moduleKey: string, schema: Schema)`

示例，创建一个 CMS 模块：

```js
import schema from './schema.js';

const moduleKey = 'CAMPAIGN_PAGE';

const campainPage = new CMSModule(moduleKey, schema);
```

#### `CMSModule.prototype.getConfigList(params: Params)`
获取模块的配置列表

#### `CMSModule.prototype.CreateConfig(params: Object)`
创建模块配置

#### `CMSModule.prototype.updateConfig(configId: number, params: Object)`
更新模块配置

#### `CMSModule.prototype.deleteConfig(configId: number)`
删除模块配置

#### `CMSModule.prototype.getConfigData(configId: number, lan: Language, , refLanguage?: string)`
获取某个模块配置在指定语言下的配置数据

#### `CMSModule.prototype.updateConfigData(configId: number, language: string, data: Object, lanData: Object)`
更新某个模块配置在指定语言下的配置数据

#### `CMSModule.prototype.publishConfigData(configId: number, language: Language)`
发布指定模块配置的数据

#### `CMSModule.prototype.unPublishConfigData(configId: number, language: Language)`
取消发布特定模块配置的数据

### `CMS.render(module: CMSModule, el: Selector, options?: RenderOptions)`
将模块渲染在某个节点上，并支持 `列表页，创建页，编辑页` 的生成，默认进入列表页。通过 `options` 可以对不同页面进行单独配置。详见：[Pages](./pages.md)

### `CMS.getModules()`
获取所有的模块

### `CMS.getLocales()`
获取内置支持的所有语言文案

### `CMS.getLocale(lan: string)`
获取指定语言文案

### `CMS.setLocale(lan: string, locale: Locale)`
设置指定语言文案
