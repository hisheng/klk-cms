## 多语言
`klk-cms` 支持文案的多语言配置，默认提供的是一份中文的文案，你可以提供自定义文案进行覆盖。

### 获取内置支持的文案
可以通过 `CMS.getLocales()` 获取所有内置支持的文案。目前内置支持中文（zh_CN）和英文（en_US）。

### 设置自定义语言文案
通过 `CMS.setLocale(lan, locale)` 设置语言文案：

```js
import en_US from './locales/en_US.json';

CMS.setLocale('en_US', en_US);
```

如果已存在内置语言文案，会进行 `merge` 合并后返回。

设置完的文案都可以通过 `this.$t()` 函数拿到。


### 使用自定义语言文案

在 `CMS.render` 的 `options` 中指定使用语言文案进行渲染：

```js
CMS.render(module, '#app', {
  locale: 'en_US'
});
```