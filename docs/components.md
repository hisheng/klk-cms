## Components
在 CMS 中，组件的定义是通过一个`描述对象`完成的，通过支持的描述属性告诉 CMS 应该如何去渲染一个组件，例如定义一个 text 组件：
```js
{
  type: 'text',
  title: 'Name',
  key: 'userName',
  value: '',
  placeholder: 'please input your name'
}
```
众多组件的定义最终形成一个 `Schema` 描述文件。

---

### 支持属性
CMS 支持以下描述属性：

- **type** 指定组件类型，支持：`text`, `textarea`, `select`, `checkbox`, `radio`, `slider`, `upload`, `markdown`, `list`, `form`, `custom`

    - `text`, `textarea`, `select`, `checkbox`, `radio`, `slider` 等组件是基于 `ElementUI` 内置的基础组件；
      
    - `upload`, `markdown` 是内置的扩展组件；`list`, `form` 是复合型组件，可实现表单多层嵌套；
      
    - `custom` 为自定义组件，需要提供自定义组件实现。

    - `list`, `form` 为复合组件。
      `list` 定义了一个可添加的组件列表，需要通过 `component` 属性指明其列表项组件；
      `form` 定义了一个子级表单，需要通过 `components` 属性指明其表单项组件。

    - 当 type 指定为 `custom` 时，代表该组件是一个自定义组件，此时需要通过 `component` 属性传递一个 Vue 组件。

- **title** 组件名称，类似 form 的 label

- **key** 组件键名

- **value** 组件键值

- **visible** 组件是否可见，默认可见，如果设置为不可见，组件将不参与 validate 验证

- **placeholder** 文本框类型适用，如 `text`, `textarea`, `select`

- **rules** 组件的校验规则，使用 [`async-validator`](https://github.com/yiminghe/async-validator)，使用示例参见：ElementUI 的[表单验证](https://element.eleme.cn/1.4/#/zh-CN/component/form#biao-dan-yan-zheng)

- **props** prop 对象，将通过 [`v-bind`](https://cn.vuejs.org/v2/api/#v-bind) 的方式传递给底层使用的 ElementUI 组件

```js
{
  type: 'text',
  title: '姓名',
  key: 'name',
  value: '',
  props: {
    // 设置不可用
    disabled: true,
    // 或者只读
    readonly: true,
  },
}
```

- **components** 当 `type` 为 `form` 时可用，指定其表单项组件

- **component** 当 `type` 为 `list` 时可用，指定其列表项组件；当 `type` 为 `custom` 时可用，指定其自定义组件。

- **cloudinaryOptions** 当 `type` 为 `upload` 时可用。由于内置 `upload` 组件使用了 cloudinary 进行资源上传，因此提供此属性对 cloudinary 进行配置。

- **onChange**
  每个组件发生改变时会向外 emit `change` 事件，触发组件定义的 `onChange` 方法  ，并接受一个对象作为参数：
  
    - component 发生变化的组件

      ⚠️注意：如果父组件定义了 onChange  方法，会不断向外冒泡

```js
{
  type: 'select',
  title: 'Sex',
  key: 'userSex',
  value: '',
  // 组件自身，或者内部子组件发生改变时触发
  // 参数 component 指向发生变化的组件（组件本身或者子组件）
  onChange: ({ component }) => {
    // do something
    // this 指向定义 onChange 的组件
  }
}
```

### 组件示例

#### text
```js
{
  type: 'text',
  title: 'Name',
  key: 'userName',
  value: '',
  placeholder: 'please input your name'
}
```

#### select
```js
{
  type: 'select',
  title: 'Sex',
  key: 'userSex',
  value: 'male',
  options: ['male', 'female'],
}
```

#### custom 自定义组件
```js
// 引入你的自定义组件
import customComp from './custom-comp.vue';

const custom = {
  type: 'custom',
  title: '自定义组件',
  key: 'foo',
  value: '',
  component: customComp
};
```

### list 列表组件
```js
{
  type: 'list',
  title: '首页轮播图',
  key: 'homeBanners',
  value: [],
  component: {
    type: 'upload',
    value: '',
  },
}
```

### form 表单组件
```js
{
  type: 'form',
  title: '用户信息',
  key: 'userInfo',
  value: [],
  components: [
    {
      type: 'text',
      title: '用户名',
      key: 'userName',
      value: '',
    },
    {
      type: 'text',
      title: '用户邮箱',
      key: 'userEmail',
      value: '',
    }
  ],
}
```
